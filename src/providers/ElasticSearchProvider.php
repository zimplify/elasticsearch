<?php
    namespace Zimplify\ElasticSearch\Providers;
    use Zimplify\Core\{Application, Instance, Provider};
    use Zimplify\Core\Interfaces\IProviderSyncEnableInterface;
    use Zimplify\Core\Services\{ArrayUtils, XmlUtils};
    use ElasticSearch\ElasticSearch\ClientBuilder;
    use \RuntimeException;
    use \SimpleXMLElement;
    
    /**
     * <provide description for this service provider>
     * @package Zimplify\ElasticSearch (code 25)
     * @type Provider (code 03)
     * @file ElasticSearchProvider (code 01)
     */
    class ElasticSearchProvider extends Provider implements IProviderSyncEnableInterface {
    
        const ARGS_HOSTS = "host";
        const ARGS_TOKEN  = "token";
        const ARGS_USER = "user";
        const CFG_ELASTIC_SETUP = "vendor.elastic";
        const DEF_CLS_NAME = "Zimplify\\ElasticSearch\\Providers\\ElasticSearchProvider";
        const DEF_NUM_OF = "number_of";
        const DEF_SHT_NAME = "elastic::elastic-search";
        const ERR_BAD_CONFIG = 400250301001;
        const ERR_BAD_TYPE = 500250301002;
        const FLD_INDEX = "index";
        const XFIELD_NAME = "name";
        const XFIELD_PROPERTIES = "properties";
        const XFIELD_TYPE = "type";
        const XPATH_GET_METADATA = "//metadata";

        private $client;

        /**
         * create the compatible ElasticSearch mapping from XML metadata
         * @param SimpleXMLElement $node the element to analyze
         * @return array
         */
        protected function analyze(SimpleXMLElement $node) : array {
            $result = [];
            if (count($node->children()) > 0) {
                $result[self::XFIELD_TYPE] = "nested";
                $properties = [];
                foreach ($node->children() as $subnode) {
                    $subattr = XmlUtils::attributes($subnode);
                    $properties[$subattr[self::XFIELD_NAME]] = $this->analyze($subnode);
                }
                $result[self::XFIELD_PROPERTIES] = $properties;
            } else {
                $attr = XmlUtils::attributes($node);
                $name = $attr[self::XFIELD_NAME];
                $type = null;
                switch ($attr[self::XFIELD_TYPE]) {
                    case "text":
                        $type = array_key_exists(self::XFIELD_INDEXED, $attr) && $attr[self::XFIELD_INDEXED] == "true" ? "keyword" : "text";
                        break;                        
                    case "number": $type = "integer"; break;
                    case "decimal": $type = "double"; break;
                    case "yesno": $type = "boolean"; break;
                    case "array":
                    case "date": $type = "date"; break;
                    case "file": 
                    default:
                        throw new RuntimeException("Unable to interpret type ".$attr[self::XFIELD_TYPE], self::ERR_BAD_TYPE);
                }
                $result[$name] = [self::XFIELD_TYPE => $type];
            }
            return $result;
        }
  
        /**
         * creating a new object on sync data source
         * @param string $object the target object 
         * @param string $key the target object key 
         * @param string $data the target object data
         * @return mixed
         */
        public function create(string $object, string $key, Instance $data) {
        }

        /**
         * deieting an object on sync data source
         * @param string $object the target object 
         * @param string $key the target object key 
         * @return mixed
         */
        public function delete(string $object, string $key) {

        }

        /**
         * generate a new index on ElasticSearch 
         * @param string $object the object to create
         * @return string
         */
        protected function generate(string $object) : string {
            $name = ArrayUtils::extract($object, "type");

            // nailing our settings
            $settings = [
                "settings" => $this->setup(),
                "mappings" => $this->map($object)
            ];
            
            // now issue the request
            $result = $this->client->indices()->create(["index" => $name, "body" => $settings]);

        }

        /**
         * initializing the provider.
         * @return void
         */
        protected function initialize() {
            parent::initialize();
            $configuration = Application::env(self::CFG_ELASTIC_SETUP);
            $host = array_key_exists(self::ARGS_HOSTS, $configuration) ? $configuration[self::ARGS_HOSTS] : null;
            $user = array_key_exists(self::ARGS_USER, $configuration) ? $configuration[self::ARGS_USER] : null;
            $token = array_key_exists(self::ARGS_TOKEN, $configuration) ? $configuration[self::ARGS_TOKEN] : null;

            if (!is_null($host)) {
                $this->client = ClientBuilder::create();
                $this->client = $this->client->setHosts($host);
                if (!is_null($token) && !is_null($user)) 
                    $this->client = $this->client->setApiKey($user, $token);
                $this->client = $this->client->build();                
            } else 
                throw new RuntimeException("Unable to initialize client due to bad configuration", self::ERR_BAD_CONFIG);

        }

        /**
         * check if an index is already created for this object
         * @param string $object the object index to validate
         * @return bool
         */
        protected function isIndexCreated(string $object) : bool {
            $result = false;
            $query = $this->client->get([self::FLD_INDEX => $object]);

            return $result;
        }        
    
        /**
         * check if all required arguments are supplied into the provider for initialization.
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }

        /**
         * mapping our object instance into types accepted by ElasticSearch
         * @param Instance $object the object to analyze
         * @return array
         */
        protected function map(Instance $object) : array {
            $result = [];
            foreach ($object->describe()->xpath(self::XPATH_GET_METADATA) as $node) {
                $attributes = XmlUtils::attributes($node);
                $name = $attributes[self::XFIELD_NAME];
                $value = $this->analyze($node);
                $result[$name] = $value;
            }
            return $result;
        } 

        /**
         * getting the basic setup of the host
         * @return array
         */
        protected function setup() : array {
            $settings = Application::env(self::CFG_ELASTIC_SETUP.".settings");
            $result = [];

            // configure sharding and replicas
            foreach ($settings as $field => $value) 
                $result[self::DEF_NUM_OF."_$field"] = $value;
            
            // return data
            return $result;
        }

        /**
         * update an object on sync data source
         * @param string $object the target object 
         * @param string $key the target object key 
         * @param string $data the target object data
         * @return mixed
         */
        public function update(string $object, string $key, Instance $data) {

        }        
    }